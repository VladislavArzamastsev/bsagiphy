package com.example.bsa_giphy.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class FileSystemUtils {

    private FileSystemUtils() {
    }

    public static void createDirectoryIfNotExists(Path path) {
        try {
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void createFileIfNotExists(Path path) {
        try {
            if (!Files.exists(path)) {
                Files.createFile(path);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Path> getDirectoryEntries(Path directory) {
        try {
            return Files.list(directory).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, List<Path>> getMapSubdirectoryNamesToNamesOfFilesInThemWithoutExtension(Path directory) {
        Map<String, List<Path>> out = new HashMap<>();
        if(! Files.exists(directory)){
            return out;
        }
        List<Path> directoryContent = getDirectoryEntries(directory);
        for(Path path: directoryContent){
            if(Files.isDirectory(path)){
                List<Path> paths = getDirectoryEntries(path);
                String keyword = path.getFileName().toString();
                out.put(keyword, paths);
            }
        }
        return out;
    }

    public static void deleteDirectoryContent(Path directory){
        try {
            List<Path> content = getDirectoryEntries(directory);
            for (Path path : content) {
                org.springframework.util.FileSystemUtils.deleteRecursively(path);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
