package com.example.bsa_giphy.util;

import com.example.bsa_giphy.entity.giphy.Gif;
import com.example.bsa_giphy.entity.giphy.Images;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.example.bsa_giphy.enums.FileTypes.GIF;

public final class GiphyGifUtils {

    private GiphyGifUtils() {
    }

    public static Gif getRandomGif(Collection<? extends Gif> gifs) {
        List<? extends Gif> filteredGifs = gifs
                .stream()
                .filter(g -> g.getType() != null && Objects.equals(g.getType().toLowerCase(), GIF.getType()))
                .collect(Collectors.toList());
        if (filteredGifs.isEmpty()) {
            throw new IllegalArgumentException("Input does not contain any gifs");
        }
        return ListUtils.getRandomElement(filteredGifs);
    }

    public static String getUrlForLoadingGif(Gif gif) {
        Images images = gif.getImages();
        if (images == null) {
            throw new RuntimeException("No images in gif");
        }
        if (images.getOriginalImage() != null) {
            return convertImageUrlToUrlWhichCanLoadGif(images.getOriginalImage().getUrl());
        } else if (images.getDownsized() != null) {
            return convertImageUrlToUrlWhichCanLoadGif(images.getDownsized().getUrl());
        }
        throw new RuntimeException("No images in gif");
    }

    private static String convertImageUrlToUrlWhichCanLoadGif(String imageUrl) {
        return imageUrl.replaceFirst("media(\\d)*", "i");
    }
}
