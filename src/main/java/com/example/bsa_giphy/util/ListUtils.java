package com.example.bsa_giphy.util;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public final class ListUtils {

    private ListUtils(){}

    public static <T> T getRandomElement(List<? extends T> list) {
        return list.get(ThreadLocalRandom.current().nextInt(list.size()));
    }
}
