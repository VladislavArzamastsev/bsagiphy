package com.example.bsa_giphy.util;

import com.example.bsa_giphy.dto.GifResponseDto;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class GifResponseDtoConverter {

    private GifResponseDtoConverter(){}

    public static List<GifResponseDto> ofMap(Map<String, List<Path>> keywordToPathsMap){
        return keywordToPathsMap.entrySet()
                .stream()
                .map(e -> new GifResponseDto(
                        e.getKey(),
                        e.getValue()
                ))
                .collect(Collectors.toList());
    }

}
