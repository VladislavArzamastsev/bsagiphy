package com.example.bsa_giphy.dao.history;

import com.example.bsa_giphy.entity.history.HistoryEntry;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.bsa_giphy.enums.StoragePaths.*;
import static com.example.bsa_giphy.enums.FileTypes.*;
import static com.example.bsa_giphy.enums.DateTimeFormatters.*;

@Repository
public final class CsvHistoryDao implements HistoryDao {

    private static final String HISTORY_FILENAME = "history".concat(CSV.getExtension());
    private final String[] headers = {"Date", "Keyword", "Path"};

    @Override
    public void write(HistoryEntry historyEntry, String userId) {
        Path destination = getPathToHistoryFile(userId);
        createFileIfNotExists(destination);
        try (FileWriter fileWriter = new FileWriter(destination.toFile(), true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
             CSVPrinter printer = new CSVPrinter(bufferedWriter, CSVFormat.DEFAULT)) {
            String date = DEFAULT_HISTORY_FORMATTER.getDateTimeFormatter().format(historyEntry.getLocalDate());
            printer.printRecord(date, historyEntry.getKeyword(), historyEntry.getPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean deleteHistory(String userId) {
        try {
            return Files.deleteIfExists(getPathToHistoryFile(userId));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<HistoryEntry> getHistory(String userId) {
        Path path = getPathToHistoryFile(userId);
        if(! Files.exists(path)){
            return new ArrayList<>();
        }
        List<CSVRecord> records;
        try(Reader in = new FileReader(path.toFile())) {
            records = CSVFormat.DEFAULT
                    .withHeader(headers)
                    .withFirstRecordAsHeader()
                    .parse(in)
                    .getRecords();
        }catch (IOException e){
            throw new RuntimeException(e);
        }
        return records
                .stream()
                .map(r -> new HistoryEntry(
                        LocalDate.parse(r.get(headers[0]), DEFAULT_HISTORY_FORMATTER.getDateTimeFormatter()),
                        r.get(headers[1]),
                        Paths.get(r.get(headers[2]))
                ))
                .collect(Collectors.toList());
    }

    private Path getPathToHistoryFile(String userId) {
        return USERS.getPath().resolve(
                userId.concat(FileSystems.getDefault().getSeparator()).concat(HISTORY_FILENAME));
    }

    //Creates csv file with headers
    private void createFileIfNotExists(Path destination) {
        if (!Files.exists(destination)) {
            try (FileWriter fileWriter = new FileWriter(destination.toFile());
                 BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                 CSVPrinter printer = new CSVPrinter(bufferedWriter, CSVFormat.DEFAULT.withHeader(headers))) {

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
