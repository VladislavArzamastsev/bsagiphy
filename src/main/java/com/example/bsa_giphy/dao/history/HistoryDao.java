package com.example.bsa_giphy.dao.history;

import com.example.bsa_giphy.entity.history.HistoryEntry;

import java.util.List;

public interface HistoryDao {

    void write(HistoryEntry historyEntry, String userId);

    boolean deleteHistory(String userId);

    List<HistoryEntry> getHistory(String userId);
}
