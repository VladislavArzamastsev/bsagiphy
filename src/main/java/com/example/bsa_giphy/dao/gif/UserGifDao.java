package com.example.bsa_giphy.dao.gif;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface UserGifDao {

    Path transferGifFromCache(Path pathToGifInCache, String userId, String keyword);

    Path getAnyGifPath(String userId, String keyword);

    Map<String, List<Path>> getAllUsersGifPaths(String userId);

    void delete(String userId);
}
