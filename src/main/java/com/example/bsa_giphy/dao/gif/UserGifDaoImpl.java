package com.example.bsa_giphy.dao.gif;

import com.example.bsa_giphy.util.FileSystemUtils;
import com.example.bsa_giphy.util.ListUtils;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;

import static com.example.bsa_giphy.enums.StoragePaths.*;

@Repository
public final class UserGifDaoImpl implements UserGifDao{

    @Override
    public Path transferGifFromCache(Path pathToGifInCache, String userId, String keyword) {
        Path target = getFullPathInUsers(userId, keyword, pathToGifInCache.getFileName());
        this.createDirectoryIfNotExists(userId, keyword);
        FileSystemUtils.createFileIfNotExists(target);
        try {
            Files.copy(pathToGifInCache, target, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return target;
    }

    @Override
    public Path getAnyGifPath(String userId, String keyword) {
        Path directoryPath = getKeywordDirectoryPath(userId, keyword);
        if(! Files.exists(directoryPath)) {
            return null;
        }
        List<Path> directoryEntries = FileSystemUtils.getDirectoryEntries(directoryPath);
        if(directoryEntries.isEmpty()){
            return null;
        }
        return ListUtils.getRandomElement(directoryEntries);
    }

    @Override
    public Map<String, List<Path>> getAllUsersGifPaths(String userId) {
        return FileSystemUtils.getMapSubdirectoryNamesToNamesOfFilesInThemWithoutExtension(getUserPath(userId));
    }

    @Override
    public void delete(String userId) {
        Path path = getUserPath(userId);
        if(! Files.exists(path)){
            return;
        }
        FileSystemUtils.deleteDirectoryContent(path);
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void createDirectoryIfNotExists(String userId, String keyword) {
        FileSystemUtils.createDirectoryIfNotExists(getKeywordDirectoryPath(userId, keyword));
    }

    private Path getUserPath(String userId){
        return USERS.getPath().resolve(userId);
    }

    private Path getKeywordDirectoryPath(String userId, String keyword){
        return getUserPath(userId).resolve(keyword);
    }

    private Path getFullPathInUsers(String userId, String keyword, Path filename){
        return getKeywordDirectoryPath(userId, keyword).resolve(filename);
    }
}
