package com.example.bsa_giphy.dao.gif;

import com.example.bsa_giphy.entity.giphy.Gif;
import com.example.bsa_giphy.entity.giphy.ResponseOnGifSearch;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

public abstract class AbstractGiphyWebGifDao implements WebGifDao{

    private final HttpClient httpClient = HttpClient.newHttpClient();

    @Override
    public final List<Gif> findGifs(String keyword) {
        try {
            HttpRequest httpRequest = buildRequest(keyword);
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return parseResponse(httpResponse).getData();
        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private ResponseOnGifSearch parseResponse(HttpResponse<String> httpResponse) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.readValue(httpResponse.body(), ResponseOnGifSearch.class);
    }

    private HttpRequest buildRequest(String keyword) throws URISyntaxException {
        return HttpRequest.newBuilder()
                .uri(new URI(getUriString(keyword)))
                .header("Content-Type", "application/json")
                .build();
    }

    protected abstract String getUriString(String keyword);

}
