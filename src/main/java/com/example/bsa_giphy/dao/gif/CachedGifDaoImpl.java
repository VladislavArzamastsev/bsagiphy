package com.example.bsa_giphy.dao.gif;

import com.example.bsa_giphy.util.FileSystemUtils;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.example.bsa_giphy.enums.StoragePaths.*;
import static com.example.bsa_giphy.enums.FileTypes.*;

@Repository
public final class CachedGifDaoImpl implements CachedGifDao {

    @Override
    public Path save(String keyword, String urlToGif, String gifId) {
        Path destination = getFullPathInCache(keyword, gifId);
        this.createDirectoryIfNotExists(keyword);
        FileSystemUtils.createFileIfNotExists(destination);
        try (InputStream in = new URL(urlToGif).openStream()) {
            Files.copy(in, destination, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return destination;
    }

    @Override
    public List<Path> getCachedGifPaths(String keyword) {
        Path directoryInCache = getDirectoryPathForKeyword(keyword);
        if (!Files.exists(directoryInCache)) {
            return new ArrayList<>();
        }
        return FileSystemUtils.getDirectoryEntries(directoryInCache);
    }

    @Override
    public Map<String, List<Path>> getAllCachedGifPaths() {
        return FileSystemUtils.getMapSubdirectoryNamesToNamesOfFilesInThemWithoutExtension(CACHE.getPath());
    }

    @Override
    public void deleteCacheOnDisk() {
        FileSystemUtils.deleteDirectoryContent(CACHE.getPath());
    }

    private void createDirectoryIfNotExists(String keyword) {
        FileSystemUtils.createDirectoryIfNotExists(getDirectoryPathForKeyword(keyword));
    }

    private Path getDirectoryPathForKeyword(String keyword) {
        return CACHE.getPath().resolve(keyword);
    }

    private Path getFullPathInCache(String keyword, String filename) {
        return getDirectoryPathForKeyword(keyword).resolve(filename.concat(GIF.getExtension()));
    }
}
