package com.example.bsa_giphy.dao.gif;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile("prod")
public final class GiphyWebGifDao extends AbstractGiphyWebGifDao {

    @Value("${giphy.api_key}")
    private String apiKey;

    protected String getUriString(String keyword) {
        return String.format(
                "https://api.giphy.com/v1/gifs/search?q=%s&api_key=%s",
                keyword, apiKey);
    }
}
