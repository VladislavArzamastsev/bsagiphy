package com.example.bsa_giphy.dao.gif;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface CachedGifDao {

    Path save(String keyword, String urlToGif, String gifId);

    List<Path> getCachedGifPaths(String keyword);

    Map<String, List<Path>> getAllCachedGifPaths();

    void deleteCacheOnDisk();

}
