package com.example.bsa_giphy.dao.gif;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile("dev")
public final class DevGiphyWebGifDao extends AbstractGiphyWebGifDao {

    @Override
    protected String getUriString(String keyword) {
        return String.format(
                "https://api.giphy.com/v1/gifs/search?q=%s&api_key=EAjN7MQYsagA2dXdkYS2LWFD1k7GKH5g",
                keyword);
    }
}
