package com.example.bsa_giphy.dao.gif;

import com.example.bsa_giphy.entity.giphy.Gif;

import java.util.List;

public interface WebGifDao {

    List<Gif> findGifs(String keyword);

}
