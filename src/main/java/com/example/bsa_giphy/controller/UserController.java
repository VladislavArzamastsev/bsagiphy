package com.example.bsa_giphy.controller;

import com.example.bsa_giphy.dto.GenerateGifRequestDto;
import com.example.bsa_giphy.dto.GifResponseDto;
import com.example.bsa_giphy.dto.HistoryEntryDto;
import com.example.bsa_giphy.service.gif.UserGifService;
import com.example.bsa_giphy.service.history.HistoryService;
import com.example.bsa_giphy.validation.GenerateGifRequestDtoValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

import static com.example.bsa_giphy.validation.RegexStrings.*;

@Controller
@Validated
@RequestMapping("/user")
public class UserController {

    private final UserGifService userGifService;
    private final HistoryService historyService;
    private final GenerateGifRequestDtoValidator generateGifRequestDtoValidator;

    public UserController(UserGifService userGifService, HistoryService historyService,
                          GenerateGifRequestDtoValidator generateGifRequestDtoValidator) {
        this.userGifService = userGifService;
        this.historyService = historyService;
        this.generateGifRequestDtoValidator = generateGifRequestDtoValidator;
    }

    @InitBinder("generateGifRequestDto")
    public void generateRequestBinder(WebDataBinder webDataBinder){
        webDataBinder.addValidators(generateGifRequestDtoValidator);
    }

    //  Получить список всех файлов с папки пользователя на диске
//  GET /user/:id/all
    @GetMapping("/{id}/all")
    public ResponseEntity<List<GifResponseDto>> getAllRecords(
            @PathVariable("id") @Pattern(regexp = VALID_FILENAME) String id) {
        List<GifResponseDto> dtos = userGifService.getAll(id);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    //  Получить историю пользователя
//  GET /user/:id/history
    @GetMapping("/{id}/history")
    public ResponseEntity<List<HistoryEntryDto>> getHistory(
            @PathVariable("id") @Pattern(regexp = VALID_FILENAME) String id) {
        List<HistoryEntryDto> out = historyService.getHistory(id);
        return new ResponseEntity<>(out, HttpStatus.OK);
    }

    //    Очистить историю пользователя
//    DELETE /user/:id/history/clean
    @DeleteMapping("/{id}/history/clean")
    public ResponseEntity<Void> deleteHistory(@PathVariable("id") @Pattern(regexp = VALID_FILENAME) String id) {
        boolean deleted = historyService.deleteHistory(id);
        HttpStatus httpStatus = deleted ? HttpStatus.NO_CONTENT : HttpStatus.OK;
        return new ResponseEntity<>(httpStatus);
    }

    // Выполнить поиск картинки. Если указан параметр force, то игнорировать кэш в памяти и сразу
// читать данные с диска. Добавить файл в кэш в памяти, если его еще там нет.
// GET /user/:id/search?query&force
    @GetMapping("/{id}/search")
    public ResponseEntity<Path> getGif(
            @PathVariable("id") @Pattern(regexp = VALID_FILENAME) String id,
            @RequestParam("query") @Pattern(regexp = VALID_FILENAME) String query,
            @RequestParam(name = "force", required = false) Boolean force) {
        Path path;
        if (force == null || Objects.equals(force, Boolean.FALSE)) {
            path = userGifService.getGif(id, query);
        } else {
            path = userGifService.getGifIgnoringCache(id, query);
        }
        if (path == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(path, HttpStatus.OK);
    }

    // Сгенерировать GIF. Если указан параметр force, то игнорировать кэш на диске (папка cache)
// и сразу искать файл в giphy.com. Добавить файл в кэш на диске (папка cache).
// (Доп. задание: избежать дублирования GIF файлов в кэше)
// POST /user/:id/generate
    @PostMapping("/{id}/generate")
    public ResponseEntity<Path> generateGif(@PathVariable("id") @Pattern(regexp = VALID_FILENAME) String id,
                                            @Validated @RequestBody GenerateGifRequestDto generateGifRequestDto,
                                            BindingResult requestBindingResult) {
        if(requestBindingResult.hasErrors()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Path path;
        String keyword = generateGifRequestDto.getQuery();
        if(generateGifRequestDto.getForce() == null
                || Objects.equals(generateGifRequestDto.getForce(), Boolean.FALSE)){
            path = userGifService.generateGif(id, keyword);
        }else {
            path = userGifService.generateGifIgnoringCache(id, keyword);
        }
        historyService.writeToHistory(id, keyword, path);
        return new ResponseEntity<>(path, HttpStatus.OK);
    }

    // Очистить кэш пользователя в памяти по ключу query. Если ключ не указан, то очистить все данные
// по пользователю в кэше в памяти.
// DELETE /user/:id/reset?query
    @DeleteMapping("/{id}/reset")
    public ResponseEntity<Void> deleteCache(
            @PathVariable("id") @Pattern(regexp = VALID_FILENAME) String id,
            @RequestParam(name = "query", required = false) @Pattern(regexp = VALID_FILENAME) String query) {
        userGifService.invalidateCaches(id, query);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    //  Удалить все данные по пользователю как на диске так и в кэше в памяти
//  DELETE /user/:id/clean
    @DeleteMapping("/{id}/clean")
    public ResponseEntity<Void> deleteAllData(@PathVariable("id") @Pattern(regexp = VALID_FILENAME) String id) {
        userGifService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
