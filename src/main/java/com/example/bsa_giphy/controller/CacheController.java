package com.example.bsa_giphy.controller;

import com.example.bsa_giphy.dto.GifResponseDto;
import com.example.bsa_giphy.dto.GenerateGifRequestDto;
import com.example.bsa_giphy.service.gif.CacheGifService;
import com.example.bsa_giphy.validation.GenerateGifRequestDtoValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.util.List;

import static com.example.bsa_giphy.validation.RegexStrings.VALID_FILENAME;

@Controller
@RequestMapping("/cache")
public final class CacheController {

    private final CacheGifService cacheGifService;
    private final GenerateGifRequestDtoValidator generateGifRequestDtoValidator;

    public CacheController(CacheGifService cacheGifService,
                           GenerateGifRequestDtoValidator generateGifRequestDtoValidator) {
        this.cacheGifService = cacheGifService;
        this.generateGifRequestDtoValidator = generateGifRequestDtoValidator;
    }

    @InitBinder("generateGifRequestDto")
    public void generateRequestBinder(WebDataBinder webDataBinder){
        webDataBinder.addValidators(generateGifRequestDtoValidator);
    }

// Получить кэш с диска. Если указан query, то выбрать только соответствующие файлы.
// GET /cache?query
    @GetMapping
    public ResponseEntity<List<GifResponseDto>> getAll(
            @RequestParam(name = "query", required = false) @Pattern(regexp = VALID_FILENAME) String query
    ){
        List<GifResponseDto> allFromCache = cacheGifService.getAllFromCache(query);
        return new ResponseEntity<>(allFromCache, HttpStatus.OK);
    }

// Скачать картинку из giphy.com и положить в соответствующую папку в кэше на диске.
// Вернуть объект со всеми картинками в кэше на диске.
// POST /cache/generate
    @PostMapping("/generate")
    public ResponseEntity<GifResponseDto> generate(
            @Validated @RequestBody GenerateGifRequestDto generateGifRequestDto,
            BindingResult bindingResult
            ){
        if(bindingResult.hasErrors()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        GifResponseDto gifResponseDto = cacheGifService.generateGifInCache(generateGifRequestDto.getQuery());
        return new ResponseEntity<>(gifResponseDto, HttpStatus.OK);
    }

//  Очистить кэш на диске
    @DeleteMapping
    public ResponseEntity<Void> deleteCacheOnDisk(){
        cacheGifService.deleteCacheOnDisk();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
