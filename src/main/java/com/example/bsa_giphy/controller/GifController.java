package com.example.bsa_giphy.controller;

import com.example.bsa_giphy.service.gif.CacheGifService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.nio.file.Path;
import java.util.List;

@Controller
@RequestMapping("/gifs")
public final class GifController {

    private final CacheGifService cacheGifService;

    public GifController(CacheGifService cacheGifService) {
        this.cacheGifService = cacheGifService;
    }

//  Получить список всех файлов без привязки к ключевым словам
//  GET /gifs
    @GetMapping
    public ResponseEntity<List<Path>> getAllGifs() {
        List<Path> paths = cacheGifService.getAllFromCache();
        return new ResponseEntity<>(paths, HttpStatus.OK);
    }
}
