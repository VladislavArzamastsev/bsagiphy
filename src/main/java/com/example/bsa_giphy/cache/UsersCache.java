package com.example.bsa_giphy.cache;

import java.nio.file.Path;

public interface UsersCache {

    void add(String userId, String keyword, Path path);

    Path getAnyPath(String userId, String keyword);

    void delete(String userId);

    void delete(String userId, String keyword);
}
