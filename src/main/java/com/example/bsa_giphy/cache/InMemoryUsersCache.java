package com.example.bsa_giphy.cache;

import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

@Component
public final class InMemoryUsersCache implements UsersCache{

    private final Map<String, Map<String, Set<Path>>> cache = new ConcurrentHashMap<>();

    @Override
    public void add(String userId, String keyword, Path path){
        Map<String, Set<Path>> cacheForUser = cache.getOrDefault(userId, new ConcurrentHashMap<>());
        Set<Path> pathsForKeyword = cacheForUser.getOrDefault(keyword, new CopyOnWriteArraySet<>());
        pathsForKeyword.add(path);
        cacheForUser.putIfAbsent(keyword, pathsForKeyword);
        cache.putIfAbsent(userId, cacheForUser);
    }

    @Override
    public Path getAnyPath(String userId, String keyword){
        Map<String, Set<Path>> cacheForUser = cache.get(userId);
        if(cacheForUser == null){
            return null;
        }
        Set<Path> pathsForKeyword = cacheForUser.get(keyword);
        if(pathsForKeyword == null){
            return null;
        }
        Iterator<Path> pathIterator = pathsForKeyword.iterator();
        if(pathIterator.hasNext()){
            return pathIterator.next();
        }
        return null;
    }

    @Override
    public void delete(String userId) {
        cache.remove(userId);
    }

    @Override
    public void delete(String userId, String keyword) {
        Map<String, Set<Path>> cacheForUser = cache.get(userId);
        if(cacheForUser != null){
            cacheForUser.remove(keyword);
        }
    }
}
