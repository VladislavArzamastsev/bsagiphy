package com.example.bsa_giphy.entity.history;

import java.nio.file.Path;
import java.time.LocalDate;

public class HistoryEntry {

    private final LocalDate localDate;
    private final String keyword;
    private final Path path;

    public HistoryEntry(LocalDate localDate, String keyword, Path path) {
        this.localDate = localDate;
        this.keyword = keyword;
        this.path = path;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public String getKeyword() {
        return keyword;
    }

    public Path getPath() {
        return path;
    }
}
