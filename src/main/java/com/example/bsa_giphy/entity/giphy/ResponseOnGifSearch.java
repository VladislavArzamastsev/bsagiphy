package com.example.bsa_giphy.entity.giphy;

import java.util.List;

public class ResponseOnGifSearch {
    private List<Gif> data;

    public ResponseOnGifSearch() {
    }

    public ResponseOnGifSearch(List<Gif> data) {
        this.data = data;
    }

    public List<Gif> getData() {
        return data;
    }

    public void setData(List<Gif> data) {
        this.data = data;
    }
}
