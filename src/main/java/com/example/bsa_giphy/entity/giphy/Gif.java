package com.example.bsa_giphy.entity.giphy;

public class Gif {

    private String id;
    private String type;
    private Images images;

    public Gif() {
    }

    public Gif(String id, String type, Images images) {
        this.id = id;
        this.type = type;
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

}
