package com.example.bsa_giphy.entity.giphy;

public class Images {

    private Image originalImage;
    private Image downsized;

    public Images() {
    }

    public Images(Image originalImage, Image downsized) {
        this.originalImage = originalImage;
        this.downsized = downsized;
    }

    public Image getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(Image originalImage) {
        this.originalImage = originalImage;
    }

    public Image getDownsized() {
        return downsized;
    }

    public void setDownsized(Image downsized) {
        this.downsized = downsized;
    }
}
