package com.example.bsa_giphy.service.history;

import com.example.bsa_giphy.dao.history.HistoryDao;
import com.example.bsa_giphy.dto.HistoryEntryDto;
import com.example.bsa_giphy.entity.history.HistoryEntry;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.bsa_giphy.enums.DateTimeFormatters.*;

@Service
public final class HistoryServiceImpl implements HistoryService{

    private final HistoryDao historyDao;

    public HistoryServiceImpl(HistoryDao historyDao) {
        this.historyDao = historyDao;
    }

    @Override
    public void writeToHistory(String userId, String keyword, Path pathToGeneratedGif) {
        HistoryEntry historyEntry = new HistoryEntry(LocalDate.now(), keyword, pathToGeneratedGif);
        historyDao.write(historyEntry, userId);
    }

    @Override
    public boolean deleteHistory(String userId) {
        return historyDao.deleteHistory(userId);
    }

    @Override
    public List<HistoryEntryDto> getHistory(String userId) {
       return historyDao.getHistory(userId)
               .stream()
               .map(e -> new HistoryEntryDto(
                       DEFAULT_HISTORY_FORMATTER.getDateTimeFormatter().format(e.getLocalDate()),
                       e.getKeyword(),
                       e.getPath().toString()
               ))
               .collect(Collectors.toList());
    }
}
