package com.example.bsa_giphy.service.history;

import com.example.bsa_giphy.dto.HistoryEntryDto;

import java.nio.file.Path;
import java.util.List;

public interface HistoryService {

    void writeToHistory(String userId, String keyword, Path pathToGeneratedGif);

    boolean deleteHistory(String userId);

    List<HistoryEntryDto> getHistory(String userId);
}
