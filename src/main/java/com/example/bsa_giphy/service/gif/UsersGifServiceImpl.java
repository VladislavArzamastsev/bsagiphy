package com.example.bsa_giphy.service.gif;

import com.example.bsa_giphy.cache.UsersCache;
import com.example.bsa_giphy.dao.gif.UserGifDao;
import com.example.bsa_giphy.dto.GifResponseDto;
import com.example.bsa_giphy.util.GifResponseDtoConverter;
import com.example.bsa_giphy.util.ListUtils;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.List;

@Service
public final class UsersGifServiceImpl implements UserGifService{

    private final UserGifDao userGifDao;
    private final CacheGifService cacheGifService;
    private final UsersCache usersCache;

    public UsersGifServiceImpl(UserGifDao userGifDao, CacheGifService cacheGifService, UsersCache usersCache) {
        this.userGifDao = userGifDao;
        this.cacheGifService = cacheGifService;
        this.usersCache = usersCache;
    }

    @Override
    public Path generateGif(String userId, String keyword) {
        List<GifResponseDto> cachedFiles = cacheGifService.getAllFromCache(keyword);
        if (cachedFiles.isEmpty()) {
            return generateGifIgnoringCache(userId, keyword);
        }

        GifResponseDto randomDto = ListUtils.getRandomElement(cachedFiles);
        Path cachedGifPath = ListUtils.getRandomElement(randomDto.getGifs());
        Path pathInUsers = userGifDao.transferGifFromCache(cachedGifPath, userId, keyword);
        usersCache.add(userId, keyword, pathInUsers);

        return pathInUsers;
    }

    /** Produces new gif and updates cache on disk and in-memory cache
     * */
    @Override
    public Path generateGifIgnoringCache(String userId, String keyword) {
        Path path = cacheGifService.insertGifToCache(keyword);
        Path pathInUsers = userGifDao.transferGifFromCache(path, userId, keyword);
        usersCache.add(userId, keyword, pathInUsers);
        return pathInUsers;
    }

    @Override
    public Path getGif(String userId, String keyword) {
        Path path = usersCache.getAnyPath(userId, keyword);
        if (path != null) {
            return path;
        }
        return getGifIgnoringCache(userId, keyword);
    }

    @Override
    public Path getGifIgnoringCache(String userId, String keyword) {
        Path pathInFileSystem = userGifDao.getAnyGifPath(userId, keyword);
        if(pathInFileSystem != null){
            usersCache.add(userId, keyword, pathInFileSystem);
        }
        return pathInFileSystem;
    }

    @Override
    public List<GifResponseDto> getAll(String userId) {
        return GifResponseDtoConverter.ofMap(userGifDao.getAllUsersGifPaths(userId));
    }

    @Override
    public void delete(String userId) {
        userGifDao.delete(userId);
        usersCache.delete(userId);
    }

    @Override
    public void invalidateCaches(String userId, String keyword) {
        if(keyword != null){
            usersCache.delete(userId, keyword);
        }else {
            usersCache.delete(userId);
        }
    }
}
