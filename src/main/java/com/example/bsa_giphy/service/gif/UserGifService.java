package com.example.bsa_giphy.service.gif;

import com.example.bsa_giphy.dto.GifResponseDto;

import java.nio.file.Path;
import java.util.List;

public interface UserGifService {

    Path generateGif(String userId, String keyword);

    Path generateGifIgnoringCache(String userId, String keyword);

    Path getGif(String userId, String keyword);

    Path getGifIgnoringCache(String userId, String keyword);

    List<GifResponseDto> getAll(String userId);

    void delete(String userId);

    void invalidateCaches(String userId, String keyword);

}
