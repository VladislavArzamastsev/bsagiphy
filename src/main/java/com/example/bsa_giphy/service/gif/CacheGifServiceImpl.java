package com.example.bsa_giphy.service.gif;

import com.example.bsa_giphy.dao.gif.CachedGifDao;
import com.example.bsa_giphy.dao.gif.WebGifDao;
import com.example.bsa_giphy.dto.GifResponseDto;
import com.example.bsa_giphy.entity.giphy.Gif;
import com.example.bsa_giphy.util.GifResponseDtoConverter;
import com.example.bsa_giphy.util.GiphyGifUtils;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public final class CacheGifServiceImpl implements CacheGifService{

    private final CachedGifDao cachedGifDao;
    private final WebGifDao webGifDao;

    public CacheGifServiceImpl(CachedGifDao cachedGifDao, WebGifDao webGifDao) {
        this.cachedGifDao = cachedGifDao;
        this.webGifDao = webGifDao;
    }

    @Override
    public void deleteCacheOnDisk() {
        cachedGifDao.deleteCacheOnDisk();
    }

    @Override
    public List<GifResponseDto> getAllFromCache(String keyword) {
        if (keyword != null && !keyword.isBlank()) {
            List<Path> cachedGifPaths = cachedGifDao.getCachedGifPaths(keyword);
            if(cachedGifPaths.isEmpty()){
                return new ArrayList<>();
            }
            return new ArrayList<>() {{
                add(new GifResponseDto(keyword, cachedGifPaths));
            }};
        }
        return GifResponseDtoConverter.ofMap(cachedGifDao.getAllCachedGifPaths());
    }

    @Override
    public GifResponseDto generateGifInCache(String keyword) {
        insertGifToCache(keyword);
        List<Path> cachedGifPaths = cachedGifDao.getCachedGifPaths(keyword);
        return new GifResponseDto(keyword, cachedGifPaths);
    }

    @Override
    public List<Path> getAllFromCache() {
        return cachedGifDao.getAllCachedGifPaths()
                .values()
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    @Override
    public Path insertGifToCache(String keyword) {
        List<Gif> gifs = webGifDao.findGifs(keyword);
        Gif gif = GiphyGifUtils.getRandomGif(gifs);
        String gifUrl = GiphyGifUtils.getUrlForLoadingGif(gif);
        return cachedGifDao.save(keyword, gifUrl, gif.getId());
    }
}
