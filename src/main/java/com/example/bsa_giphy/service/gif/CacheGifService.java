package com.example.bsa_giphy.service.gif;

import com.example.bsa_giphy.dto.GifResponseDto;

import java.nio.file.Path;
import java.util.List;

public interface CacheGifService {

    void deleteCacheOnDisk();

    List<GifResponseDto> getAllFromCache(String keyword);

    GifResponseDto generateGifInCache(String keyword);

    List<Path> getAllFromCache();

    Path insertGifToCache(String keyword);
}
