package com.example.bsa_giphy.validation;

import com.example.bsa_giphy.dto.GenerateGifRequestDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static com.example.bsa_giphy.validation.RegexStrings.*;

import java.util.Objects;

@Component
public final class GenerateGifRequestDtoValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Objects.equals(clazz, GenerateGifRequestDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        GenerateGifRequestDto dto = (GenerateGifRequestDto) target;
        if(dto.getQuery() == null || !dto.getQuery().matches(VALID_FILENAME)){
            errors.reject("", "Invalid id");
        }
    }
}
