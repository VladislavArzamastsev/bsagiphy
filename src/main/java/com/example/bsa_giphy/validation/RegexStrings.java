package com.example.bsa_giphy.validation;

// I can't use enum, because @Regex in controller requires string to be a constant
public final class RegexStrings {

    public static final String VALID_FILENAME = "[^\\s=|`+@:\"'!$/?*><{}&%#\\\\]+";

    private RegexStrings(){}

}
