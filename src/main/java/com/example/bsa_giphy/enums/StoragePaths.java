package com.example.bsa_giphy.enums;

import java.nio.file.Path;
import java.nio.file.Paths;

public enum StoragePaths {

    CACHE(Paths.get("").toAbsolutePath().resolve("bsa_giphy/cache")),
    USERS(Paths.get("").toAbsolutePath().resolve("bsa_giphy/users"));

    private final Path path;

    StoragePaths(Path path) {
        this.path = path;
    }

    public Path getPath() {
        return path;
    }
}
