package com.example.bsa_giphy.enums;

import java.time.format.DateTimeFormatter;

public enum DateTimeFormatters {

    DEFAULT_HISTORY_FORMATTER(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

    private final DateTimeFormatter dateTimeFormatter;

    DateTimeFormatters(DateTimeFormatter dateTimeFormatter) {
        this.dateTimeFormatter = dateTimeFormatter;
    }

    public DateTimeFormatter getDateTimeFormatter() {
        return dateTimeFormatter;
    }
}
