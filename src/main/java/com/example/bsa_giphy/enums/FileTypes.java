package com.example.bsa_giphy.enums;

public enum FileTypes {

    GIF("gif"),
    CSV("csv");

    private final String type;
    private final String extension;

    FileTypes(String type) {
        this.type = type;
        this.extension = ".".concat(type);
    }

    public String getType() {
        return type;
    }

    public String getExtension() {
        return extension;
    }
}
