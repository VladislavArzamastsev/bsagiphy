package com.example.bsa_giphy;

import com.example.bsa_giphy.enums.StoragePaths;
import com.example.bsa_giphy.util.FileSystemUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@SpringBootApplication
@ConfigurationPropertiesScan
public class BsaGiphyApplication{

    static {
        initStorages();
    }

    public static void main(String[] args) {
        SpringApplication.run(BsaGiphyApplication.class, args);
    }

    private static void initStorages() {
        FileSystemUtils.createDirectoryIfNotExists(StoragePaths.CACHE.getPath());
        FileSystemUtils.createDirectoryIfNotExists(StoragePaths.USERS.getPath());
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }

}
