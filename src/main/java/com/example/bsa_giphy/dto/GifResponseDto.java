package com.example.bsa_giphy.dto;

import java.nio.file.Path;
import java.util.List;

public class GifResponseDto {

    private String query;
    private List<Path> gifs;

    public GifResponseDto() {
    }

    public GifResponseDto(String query, List<Path> gifs) {
        this.query = query;
        this.gifs = gifs;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<Path> getGifs() {
        return gifs;
    }

    public void setGifs(List<Path> gifs) {
        this.gifs = gifs;
    }
}
