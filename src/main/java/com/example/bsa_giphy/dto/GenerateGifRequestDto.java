package com.example.bsa_giphy.dto;

public class GenerateGifRequestDto {

    private String query;
    private Boolean force;

    public GenerateGifRequestDto() {
    }

    public GenerateGifRequestDto(String query, Boolean force) {
        this.query = query;
        this.force = force;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Boolean getForce() {
        return force;
    }

    public void setForce(Boolean force) {
        this.force = force;
    }
}
