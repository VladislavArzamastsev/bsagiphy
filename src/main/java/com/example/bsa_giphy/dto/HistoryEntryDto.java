package com.example.bsa_giphy.dto;

public class HistoryEntryDto {

    private String date;
    private String query;
    private String gif;

    public HistoryEntryDto() {
    }

    public HistoryEntryDto(String date, String query, String gif) {
        this.date = date;
        this.query = query;
        this.gif = gif;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getGif() {
        return gif;
    }

    public void setGif(String gif) {
        this.gif = gif;
    }
}
